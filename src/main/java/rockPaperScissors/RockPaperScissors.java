package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        boolean continueGame = true;

        while (continueGame) {
            System.out.println("Let's play round " + roundCounter);
            String humanMove = (readInput("Your choice (Rock/Paper/Scissors)?")).toLowerCase();
            
            while (! validateInput(humanMove)) {
                System.out.println("I do not understand " + humanMove + ". Could you try again?");
                humanMove = (readInput("Your choice (Rock/Paper/Scissors)?")).toLowerCase();
            }
            String computerMove = randomChoices();
            
            if (isWinner(humanMove, computerMove)) {
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n", humanMove, computerMove);
                humanScore++;
            }
            else if (isWinner(computerMove, humanMove)) {
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", humanMove, computerMove);
                computerScore++;
            }
            else {
                System.out.printf("Human chose %s, computer chose %s. It's a tie!\n", humanMove, computerMove);
            }
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);

            continueGame = continuePlaying();

            if (! continueGame) {
                System.out.println("Bye bye :)");

            }
            roundCounter++;
            

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    public String randomChoices() {
        /** valget til pcen */
        Random rand = new Random();
        String computer = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        return computer;
    }
    public boolean isWinner(String move1, String move2) {
        if (move1.equals("rock")) {
            return move2.equals("scissors");
        }
        else if (move1.equals("paper")) {
            return move2.equals("rock");
        }
        else {
            return move2.equals("paper");
        }
    }
    public boolean continuePlaying(){
        String continueplaying = readInput("Do you wish to continue playing? (y/n)?");
        return (continueplaying.equals("y"));
    }
    public static boolean validateInput(String humanMove) {
        return (humanMove.equals("rock") || humanMove.equals("scissors") || humanMove.equals("paper"));
    }
 
}
